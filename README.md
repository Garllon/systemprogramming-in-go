### Systemprogramming in Go

Schritt für Schritt die Buchteile

einfacher Hello world

## Tags

- [1.3](https://bitbucket.org/Garllon/systemprogramming-in-go/commits/tag/1.3): Nutzung von Konstanten und Packages
- [2.0](https://bitbucket.org/Garllon/systemprogramming-in-go/commits/tag/2.0): Umstellen der Methoden auf Person struct and PersonTable struct calls
- [2.1](https://bitbucket.org/Garllon/systemprogramming-in-go/commits/tag/2.1): Neue Methoden hinzufügen
- [2.1.1](https://bitbucket.org/Garllon/systemprogramming-in-go/commits/tag/2.1.1): Readme anpassen