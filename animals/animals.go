package animals

import "fmt"

type Duck interface {
	Quack()
	Walk()
}

func DuckDance(duck Duck) {
	duck.Quack()
	duck.Walk()
	duck.Quack()
	duck.Walk()
	duck.Quack()
	duck.Walk()
}

type Loon struct {
	IsSwimming bool
}

func (l *Loon) Quack() {
	fmt.Println("Quuuaack!")
}

// func (l *Loon) Swim() {
// 	l.IsSwimming = true
// }

func (l Loon) SwimWithoutPointer() {
	l.IsSwimming = true
}

func (l *Loon) Walk() {
	l.IsSwimming = false
}
