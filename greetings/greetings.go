package greetings

import (
	"fmt"
	"runtime"
)

const (
	Unknown = iota
	Female
	Male
)

type Gender int
type PersonTable map[string]*Person
type PersonMapFunc func(*Person)

type Person struct {
	id        string
	gender    Gender
	firstName string
	lastName  string
	Buddies   PersonTable
}

func (pt PersonTable) Contains(id string) bool {
	_, ok := pt[id]

	return ok
}

func (pt PersonTable) Add(person *Person) bool {
	if pt.Contains(person.id) {
		return false
	}

	pt[person.id] = person

	return true
}

func (pt PersonTable) Map(pmf PersonMapFunc) {
	for _, p := range pt {
		pmf(p)
	}
}

func (p *Person) GreetBuddies() int {
	count := 0

	p.Buddies.Map(func(buddy *Person) {
		buddy.Hello(true)

		count++
	})

	return count
}

func (p *Person) ByeBye() {
	fmt.Printf("Bye Bye, %v", p.firstName)
}

func NewPerson(id string, firstName string, lastName string, gender Gender) *Person {
	if len(id) == 0 {
		return nil
	}

	person := new(Person)

	person.id = id
	person.firstName = firstName
	person.lastName = lastName
	person.gender = gender
	person.Buddies = NewPersonTable()

	runtime.SetFinalizer(person, func(p *Person) {
		p.ByeBye()
	})

	return person
}

func NewPersonTable() PersonTable {
	return make(PersonTable)
}

func (p *Person) Hello(buddy bool) {
	if buddy {
		fmt.Printf("Hello, %s!\n", p.firstName)
	} else {
		s := p.salutation()
		fmt.Printf("Hello, %s %s!\n", s, p.lastName)
	}
}

func (p *Person) salutation() string {
	switch p.gender {
	case Female:
		return "Mrs."
	case Male:
		return "Mr."
	}

	return ""
}
