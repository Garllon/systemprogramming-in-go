package main

import (
	"./animals"
	"./greetings"
	"fmt"
)

func main() {
	var personBennet *greetings.Person = greetings.NewPerson("BP", "Bennet", "Garllon", greetings.Male)
	var personBasti *greetings.Person = greetings.NewPerson("BP", "Basti", "Mann", greetings.Male)
	var personNina *greetings.Person = greetings.NewPerson("BP", "Nina", "Frau", greetings.Female)

	buddies := personBennet.Buddies
	buddies.Add(personBasti)
	buddies = personBennet.Buddies
	buddies.Add(personNina)

	personBennet.Hello(true)
	personBennet.Hello(false)

	greetingsBuddiesCount := personBennet.GreetBuddies()
	fmt.Printf("Gegrüßt Buddies: %v\n", greetingsBuddiesCount)

	Check(nil)
	Check(1)
	Check(2.3)
	Check(personBennet)
	Check("DEFAULT VALUE")

	fmt.Println("")
	fmt.Println("ANIMALS")

	var loon = new(animals.Loon)

	animals.DuckDance(loon)
}

func Check(what interface{}) {
	switch value := what.(type) {
	case nil:
		fmt.Printf("Nothing to Check!\n")
	case int:
		fmt.Printf("This is an Integer, with the value %v\n", value)
	case float64:
		fmt.Printf("This is a Float, with the value %v\n", value)
	case *greetings.Person:
		value.Hello(true)
		value.Hello(false)
	default:
		fmt.Printf("I dont know the type you give me.\n")
	}
}
